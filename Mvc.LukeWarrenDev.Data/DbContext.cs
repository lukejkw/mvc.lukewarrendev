﻿using Mvc.LukeWarrenDev.Contracts;
using Mvc.LukeWarrenDev.Data.Contracts;
using Mvc.LukeWarrenDev.Data.Domains;
using Mvc.LukeWarrenDev.Mappings;
using System;
using System.Data.Entity;
using System.Linq;

namespace Mvc.LukeWarrenDev.Data
{
    public class LwdDbContext : DbContext
    {
        #region Data Sets

        public virtual DbSet<PortfolioItem> Orders { get; set; }

        #endregion

        #region Overrides

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PortfolioItemMapping());
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var changeSet = ChangeTracker.Entries<IAuditable>();
            if (changeSet != null)
            {
                foreach (var entry in changeSet.Where(c => c.State != EntityState.Unchanged))
                {
                    // Update tracking info for auditable objects
                    var now = DateTime.Now;
                    if (entry.Entity.CreatedOnDate == DateTime.MinValue
                        || entry.Entity.CreatedOnDate == null)
                        entry.Entity.CreatedOnDate = now;
                    entry.Entity.LastChangedOnDate = now;
                }
            }
            return base.SaveChanges();
        }

        #endregion

        #region Constructors and Initializers

        public LwdDbContext() : base("DefaultConnection")
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public static LwdDbContext Create()
        {
            return new LwdDbContext();
        }

        #endregion
    }
}