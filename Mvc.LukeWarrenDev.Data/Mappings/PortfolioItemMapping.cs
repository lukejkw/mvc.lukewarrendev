﻿using Mvc.LukeWarrenDev.Data.Domains;
using System.Data.Entity.ModelConfiguration;

namespace Mvc.LukeWarrenDev.Mappings
{
    public class PortfolioItemMapping : EntityTypeConfiguration<PortfolioItem>
    {
        public PortfolioItemMapping()
        {
            HasKey(p => p.Id);

            Property(o => o.HtmlDescription)
                .HasMaxLength(2000)
                .HasColumnType("NVARCHAR")
                .IsRequired();
            Property(o => o.ShortDesc)
                .HasMaxLength(255)
                .HasColumnType("NVARCHAR")
                .IsRequired();
        }
    }
}