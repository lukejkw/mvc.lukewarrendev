﻿using System.Collections.Generic;

namespace Mvc.LukeWarrenDev.Data.Domains
{
    public class PortfolioItem : AuditableBase
    {
        public string Slug { get; set; }
        public string Title { get; set; }
        public virtual IEnumerable<string> Images { get; set; }
        public string ShortDesc { get; set; }
        public string HtmlDescription { get; set; }
        public virtual IEnumerable<string> Technologies { get; set; }
        public string SeeLiveLink { get; set; }

        public PortfolioItem()
            : base()
        { }
    }
}