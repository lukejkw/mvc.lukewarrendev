﻿using Mvc.LukeWarrenDev.Data.Contracts;
using System;

namespace Mvc.LukeWarrenDev.Data.Domains
{
    public abstract class AuditableBase : DomainBase, IAuditable
    {
        public DateTime CreatedOnDate { get; set; }

        public DateTime LastChangedOnDate { get; set; }

        public AuditableBase()
        {
            CreatedOnDate = DateTime.MinValue;
            LastChangedOnDate = DateTime.MinValue;
        }
    }
}