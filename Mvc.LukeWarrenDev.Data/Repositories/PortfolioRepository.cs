﻿using Mvc.LukeWarrenDev.Data.Contracts;
using Mvc.LukeWarrenDev.Data.Domains;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Mvc.LukeWarrenDev.Repositories
{
    public class PortfolioRepository : RepositoryBase<PortfolioItem>, IPortfolioRepository
    {
        public PortfolioRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<PortfolioItem> GetTopItems()
        {
            throw new NotImplementedException();
        }
    }
}