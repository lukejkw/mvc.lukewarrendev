﻿using Mvc.LukeWarrenDev.Contracts;
using Mvc.LukeWarrenDev.Data.Contracts;
using Mvc.LukeWarrenDev.Repositories;

namespace Mvc.LukeWarrenDev.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        public IPortfolioRepository PortfolioItems { get; private set; }

        private readonly LwdDbContext _context;

        public UnitOfWork(LwdDbContext context)
        {
            _context = context;
            PortfolioItems = new PortfolioRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}