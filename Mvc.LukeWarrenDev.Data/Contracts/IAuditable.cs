﻿using System;

namespace Mvc.LukeWarrenDev.Data.Contracts
{
    public interface IAuditable
    {
        DateTime LastChangedOnDate { get; set; }
        DateTime CreatedOnDate { get; set; }
    }
}