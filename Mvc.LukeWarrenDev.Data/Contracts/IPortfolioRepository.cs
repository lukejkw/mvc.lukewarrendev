﻿using Mvc.LukeWarrenDev.Data.Domains;
using System.Collections.Generic;

namespace Mvc.LukeWarrenDev.Data.Contracts
{
    public interface IPortfolioRepository : IRepository<PortfolioItem>
    {
        IEnumerable<PortfolioItem> GetTopItems();
    }
}