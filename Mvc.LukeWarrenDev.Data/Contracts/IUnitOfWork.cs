﻿using Mvc.LukeWarrenDev.Data.Contracts;
using System;

namespace Mvc.LukeWarrenDev.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        IPortfolioRepository PortfolioItems { get; }

        int Complete();
    }
}