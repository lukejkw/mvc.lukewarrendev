﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc.LukeWarrenDev.Models
{
    public class ReviewItemViewModel
    {
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string ReviewerName { get; set; }
        public string ReviewText { get; set; }
    }
}