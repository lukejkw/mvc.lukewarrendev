﻿using System.Collections.Generic;

namespace Mvc.LukeWarrenDev.Models
{
    public class PortfolioItemModel
    {
        public int ItemId { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string ShortDesc { get; set; }
        public string HtmlDescription { get; set; }
        public List<string> Technologies { get; set; }
        public string SeeLiveLink { get; set; }
    }
}