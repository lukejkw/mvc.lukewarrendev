﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mvc.LukeWarrenDev.Models
{
    public class ContactViewModel
    {
        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string First { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string Last { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        [EmailAddress]
        public string Email { get; set; }

        [MinLength(2)]
        [MaxLength(255)]
        public string Message { get; set; }
    }
}