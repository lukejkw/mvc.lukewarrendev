﻿using System.Collections.Generic;

namespace Mvc.LukeWarrenDev.Models
{
    public class HomeViewModel
    {
        public List<PortfolioItemModel> PortfolioItems { get; set; }
        public List<ReviewItemViewModel> Reviews { get; set; }

        public void Populate()
        {
            PortfolioItems = new List<PortfolioItemModel>()
            {
                new PortfolioItemModel()
                {
                    Title = "ATKV Website",
                    Image = "/Content/img/portfolio/atkv_desktop_thumb.png",
                    ShortDesc = "ATKV is a massive organisation. This meant building a massive custom DNN website",
                    SeeLiveLink = "https://www.atkv.org.za/",
                    Technologies = new List<string>()
                    {
                        "DNN",
                        "C#",
                        "ASP.NET",
                    }
                },
                new PortfolioItemModel()
                {
                    Title = "Soek 'n Tak DNN Module",
                    Image = "/Content/img/portfolio/soek_n_tak_desktop.png",
                    ShortDesc = "Soek 'n Tak DNN module extension for ATKV",
                    SeeLiveLink = "https://www.atkv.org.za/Gemeenskap/Soek-n-Tak",
                    Technologies = new List<string>()
                    {
                        "DNN",
                        "C#",
                        "ASP.NET",
                        "Web API",
                        "KnockoutJS"
                    }
                },

                new PortfolioItemModel()
                {
                    Title = "Fidelity Document Builder and Management System",
                    Image = "/Content/img/portfolio/fidelity_docbuilder_desktop.png",
                    ShortDesc = "A PDF document builder and managerment system",
                    SeeLiveLink = string.Empty, // Dont really want anyone going there
                    Technologies = new List<string>()
                    {
                        "DNN",
                        "C#",
                        "ASP.NET",
                        "PDFSharp",
                        "WebForms",
                        "DNN File Manager",
                        "Granualar Permissions",
                        "KnockoutJS"
                    }
                },
                new PortfolioItemModel()
                {
                    Title = "Lapa Online Book Store",
                    Image = "/Content/img/portfolio/lapa_detail_desktop.png",
                    ShortDesc = "LAPA Uitgiewer is an Afrikaans book publishing company. Check out this DNN based eCommerce store",
                    SeeLiveLink = "https://www.lapa.co.za/",
                    Technologies = new List<string>()
                    {
                        "DNN",
                        "C#",
                        "ASP.NET",
                        "Pastel Integration",
                        "WebForms",
                        "Knockoutjs",
                        "Web API"
                    }
                },
                new PortfolioItemModel()
                {
                    Title = "DCD Project Manager",
                    Image = "/Content/img/portfolio/dcd.png",
                    ShortDesc = "Project Management system DNN extension",
                    SeeLiveLink = string.Empty,
                    Technologies = new List<string>()
                    {
                        "DNN",
                        "C#",
                        "ASP.NET",
                        "WebForms",
                    }
                },
                new PortfolioItemModel()
                {
                    Title = "Bonitas Website",
                    Image = "/Content/img/portfolio/bonitas.png",
                    ShortDesc = "Bonitas website and member portal",
                    SeeLiveLink = "http://www.bonitas.co.za/",
                    Technologies = new List<string>()
                    {
                        "DNN",
                        "C#",
                        "ASP.NET",
                        "WebForms",
                        "AngularJS"
                    }
                },
                new PortfolioItemModel()
                {
                    Title = "Properteez Mobile App",
                    Image = "/Content/img/portfolio/properteez.jpg",
                    ShortDesc = "An Android and iOS property advertisment App",
                    SeeLiveLink = string.Empty, // No website to show
                    Technologies = new List<string>()
                    {
                        "DNN",
                        "C#",
                        "ASP.NET",
                        "WebForms",
                        "Xamarin",
                        "Xamarin.Forms",
                        "Web API",
                        "SQLite"
                    }
                }
            };
            // Populate Reviews
            Reviews = new List<ReviewItemViewModel>()
            {
                new ReviewItemViewModel()
                {
                    ReviewerName = "Yvette Hough (Graphic Designer)",
                    Title = "Dedicated, experienced",
                    ImageUrl = "/Content/img/review_yvette.jpg",
                    ReviewText = "Luke is a dedicated, experienced, software development professional with vast experience and perspicacity in all aspects of development. He is also an excellent team player, with high integrity and work ethics. He remains on the forefront of digital technology and implementation standards. His skills are constantly being updated by his keen willingness to learn and absorb.  Luke can make great contributions to any company."
                },
                new ReviewItemViewModel()
                {
                    ReviewerName = "Dewald Wessels (Web Designer & Developer)",
                    Title = "Professional developer striving to provide the best",
                    ImageUrl = "/Content/img/review_dewald.jpg",
                    ReviewText = "In the time I got to know Luke at Syncrony he has proven himself as a professional developer striving to provide the best possible solution at all times, irrespective of the project or its difficulties. He has some mind boggling developments in a very short period of time leaving clients amazed."
                }
            };
        }
    }
}