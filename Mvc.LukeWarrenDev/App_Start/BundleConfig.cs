﻿using System.Web.Optimization;

namespace Mvc.LukeWarrenDev
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval")
                .Include("~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/skin")
                .Include("~/Scripts/navigation.js", "~/Scripts/google.js"));
            bundles.Add(new ScriptBundle("~/bundles/modernizr")
                .Include("~/Scripts/modernizr-*"));
            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/css/site.css",
                        "~/Content/css/font-awesome.min.css"));
        }
    }
}