﻿using Mvc.LukeWarrenDev.Models;
using System.Web.Mvc;

namespace Mvc.LukeWarrenDev.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new HomeViewModel();
            model.Populate();
            return View(model);
        }
    }
}