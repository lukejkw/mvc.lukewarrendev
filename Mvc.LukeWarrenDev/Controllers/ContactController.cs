﻿using Mvc.LukeWarrenDev.Models;
using System;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace Mvc.LukeWarrenDev.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Index()
        {
            ViewBag.InitialLoad = true;
            return View();
        }

        [HttpPost]
        public ActionResult Save(ContactViewModel model)
        {
            if (model == null || String.IsNullOrEmpty(model.First) ||
                String.IsNullOrEmpty(model.Last) ||
                String.IsNullOrEmpty(model.Email))
            {
                ViewBag.MessageSuccess = false;
            }
            else
            {
                try
                {
                    string subject = "Luke Warren Dev Enquiry Confirmation";
                    string body = $"Hi { model.First} { model.Last}.\n\n Thank you for getting in touch.I will get back to you ASAP.\n\nPlease do not reply to this message.\n\nLove Mail Robot";
                    MailMessage mail = new MailMessage("robot@lukewarrendev.co.za", model.Email, subject, body);
                    mail.Bcc.Add(new MailAddress("luke@lukewarrendev.co.za"));
                    SmtpClient smtpClient = new SmtpClient("mail.lukewarrendev.co.za", 25);
                    smtpClient.Credentials = new NetworkCredential("robot@lukewarrendev.co.za", "66542Luke92!");
                    smtpClient.Send(mail);
                    ViewBag.MessageSuccess = true;
                }
                catch (Exception ex)
                {
                    ViewBag.MessageSuccess = false;
                }
            }
            ViewBag.InitialLoad = false;
            return View("Index", model);
        }
    }
}