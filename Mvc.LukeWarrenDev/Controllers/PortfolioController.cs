﻿using Mvc.LukeWarrenDev.Models;
using System.Web.Mvc;

namespace Mvc.LukeWarrenDev.Controllers
{
    public class PortfolioController : Controller
    {
        public ActionResult Index()
        {
            // Init Model
            var model = new HomeViewModel();
            model.Populate();
            return View(model);
        }

        public ActionResult Detail(int itemId)
        {
            // TODO
            // Get detail item via id
            // Return detail model to view
            return View();
        }
    }
}