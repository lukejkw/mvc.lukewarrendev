﻿$(function () {
    init();

    function init() {
        $(".liveHtml").typed({
            strings: [
                "code",
                "design",
                "learn"
            ],
            typeSpeed: 150,
            startDelay: 1000,
            loop: true,
            loopCount: false
        });

        $(".owl-carousel").owlCarousel({
            items: 1,
            singleItem: true,
            loop: true,
            nav: false,
            dots: false,
            autoplay: true,
            autoplayHoverPause: true,
            animateOut: 'fadeOutUp',
            animateIn: 'fadeInUp',
            navigation: false,
            navText: ['<span class="fa fa-chevron-left"></span>', '<span class="fa fa-chevron-right"></span>'],
            onChange: function () { }
        });
    }
});