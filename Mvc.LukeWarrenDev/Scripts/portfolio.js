﻿$(function () {
    //var wall = new Freewall('.portfolio');
    // wall.fitWidth();

    var wall = new Freewall(".portfolio");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 150,
        cellH: 'auto',
        onResize: function () {
            wall.fitWidth();
        }
    });

    var images = wall.container.find('.item');
    images.find('img').load(function () {
        wall.fitWidth();
    });
    wall.fitWidth();
});