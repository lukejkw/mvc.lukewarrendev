﻿$(function () {
    // Mobile Menu buttons
    var $closeBtn = $('#close-menu');
    var $menuBtn = $('#menu-toggle');
    var $nav = $('#mobile-nav,#mobile-nav-links');

    // Event Actions
    var toggleMenu = function () {
        if ($('#mobile-nav:visible').length == 0) {
            // Show
            $nav.slideDown();
        } else {
            // Hide
            $nav.slideUp();
        }
    };

    // Attach events
    $closeBtn.click(toggleMenu);
    $menuBtn.click(toggleMenu);
});